using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHome : PanelBase
{
    public PanelBase panelHome = null;
    public PanelBase panelGameSelect = null;
    public PanelBase panelMissionDPA = null;
    public PanelBase panelConfig = null;
    public PanelBase panelBuy = null;
    public ParentalGate parentalGate = null;

    public Stack<PanelBase> panelStack = null;

    [Header("Config")]
    public Sprite toggleOn;
    public Sprite toggleOff;
    public Image sfxToggle;
    public Image bgmToggle;
    public Image cameraToggle;

    private void Start()
    {
        panelStack = new Stack<PanelBase>();
        panelHome?.Show();
        panelStack.Push(panelHome);
        AudioManager.Instance.PlaySoundtrack();
    }

    public void OnBackButton()
    {
        panelStack.Pop().Hide(false);
    }

    public void OnPlayButton()
    {
        NavigationManager.Instance.defaultScreen.SetActive(false);
        panelGameSelect?.Show();
        panelStack.Push(panelGameSelect);
    }

    public void OnQuitButton()
    {
        Application.Quit();
    }

    public void OnGameModeButton(int i)
    {
        if (i == 0)
        {
            panelMissionDPA?.Show();
            panelStack.Push(panelMissionDPA);
        }
        else
        {

        }
    }

    public void OnHelpButton()
    {
        panelConfig?.Show();
        panelStack.Push(panelConfig);
        sfxToggle.sprite = AudioManager.Instance.GetSFXStatus() ? toggleOff : toggleOn;
        bgmToggle.sprite = AudioManager.Instance.GetBGMStatus() ? toggleOff : toggleOn;
        cameraToggle.sprite = Application.HasUserAuthorization(UserAuthorization.WebCam) ? toggleOn : toggleOff;
    }

    public void OnStartMissionButton()
    {
        //START GAME
        LoaderManager.Instance.OnLoadingRequest(1,this);
    }

    public void OnStartLensButton()
    {
        LoaderManager.Instance.OnLoadingRequest(3, this);
    }

    public void OnNeedGameButton()
    {
        panelBuy?.Show();
        panelStack.Push(panelBuy);
    }

    public void OnMuteSFX()
    {
        sfxToggle.sprite = sfxToggle.sprite == toggleOn ? toggleOff : toggleOn;
        AudioManager.Instance.MuteSFX();
    }

    public void OnMuteBGM()
    {
        bgmToggle.sprite = bgmToggle.sprite == toggleOn ? toggleOff : toggleOn;
        AudioManager.Instance.MuteBGM();
    }

    public void OnPermissionCamera()
    {
        if (cameraToggle.sprite == toggleOn)
        {
            cameraToggle.sprite = toggleOff;
        }
        else
        {
            StartCoroutine(RequestPermissionCammera());
        }
    }

    IEnumerator RequestPermissionCammera()
    {

        Application.RequestUserAuthorization(UserAuthorization.WebCam);
        yield return null;
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            cameraToggle.sprite = toggleOn;
        }
    }

    public void SetParentalGate(string gateArea)
    {
        parentalGate.gameObject.SetActive(true);
        parentalGate.GateArea = gateArea;
    }
}
