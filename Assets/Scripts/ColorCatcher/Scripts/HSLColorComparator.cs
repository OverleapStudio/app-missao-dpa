﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HSLColorComparator{

    public static List<string> GetMatchingColorName(HueColorsList colorsList ,Color sample){
        HSLColor sampleHSL = ColorUtils.HSLfromColor(sample);
        List<string> result = new List<string>();
        result = colorsList.GetMatchingHSLColor(sampleHSL);
        if (sampleHSL.l <= colorsList.blackThreshold)
        {
            result.Add("black");
        }
        if (sampleHSL.l >= colorsList.whiteThreshold)
        {
            result.Add("white");
        }
        if (sampleHSL.s <= colorsList.greyThreshold)
        {
            result.Add("grey");
        }
        return result;
    }
}
