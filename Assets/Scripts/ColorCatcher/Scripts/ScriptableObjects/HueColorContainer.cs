﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HueColorContainer {
    
    public string Name;
    public ColorPropertyRange[] HueRanges;
    public bool useSaturation = false;
    public ColorPropertyRange saturationRange;
    public bool useLightness = false;
    public ColorPropertyRange lightnessRange;
    public RangeInt range = new RangeInt(0, 101);

    private bool compareWithHue(float hueValue){
        foreach(ColorPropertyRange range in HueRanges){
            if(range.isInside(hueValue)){
                return true;
            }
        }
        return false;
    }

    public bool compareWith(HSLColor hsl){
        if(!compareWithHue(hsl.h)){
            return false;
        }

        if(useSaturation && !saturationRange.isInside(hsl.s * saturationRange.end)){
            return false;
        }

        if (useLightness && !lightnessRange.isInside(hsl.l * lightnessRange.end))
        {
            return false;
        }

        return true;
    }

}
