﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorPropertyRange {
    public float begin = 0;
    public float end = 360;

    public bool isInside(float value){
        if(begin <= value && value <= end){
            return true;
        }else{
            return false;
        }
    }

    public ColorPropertyRange(float begin, float end){
        this.end = end;
        this.begin = begin;
    }

}
