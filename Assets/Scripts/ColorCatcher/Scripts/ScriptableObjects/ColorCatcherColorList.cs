﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Linq;
#endif

namespace PKGames.ColorCatcher
{
    [CreateAssetMenu(fileName = "GameColors", menuName = "Explorer/ColorCatcher/ColorCatcherColorList", order = 0)]
    public class ColorCatcherColorList : ScriptableObject
    {
        public HueColorsList hueColorsList;
        public List<ColorExample> colorsList = new List<ColorExample>();
    }

    [System.Serializable]
    public class ColorExample
    {

        public ColorExample(string key, string name){
            this.hueColorKey = key;
            this.colorName = name;
        }

        public string hueColorKey;
        public string colorName;
        public Color example = new Color(1,1,1,1);
        public AudioClip narrationSound;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ColorCatcherColorList))]
    class ColorCatcherColorListCustomEditor : Editor{

        public override void OnInspectorGUI()
        {
            ColorCatcherColorList script = (ColorCatcherColorList)target;
            if(GUILayout.Button("Update Colors"))
            {
                CreateNewKeys(script);
                RemoveUnecessaryKeys(script);

            }
            DrawDefaultInspector();
        }

        private static void CreateNewKeys(ColorCatcherColorList script)
        {
            foreach (HueColorContainer hcc in script.hueColorsList.knownColors)
            {
                if (!script.colorsList.Any((color) => color.hueColorKey == hcc.Name))
                {
                    script.colorsList.Add(new ColorExample(hcc.Name, hcc.Name));
                }
            }
        }

        private static void RemoveUnecessaryKeys(ColorCatcherColorList script)
        {
            var deletable = new List<ColorExample>();
            foreach (ColorExample ce in script.colorsList)
            {
                if (!script.hueColorsList.knownColors.Any((colorContainer) => colorContainer.Name == ce.hueColorKey))
                {
                    deletable.Add(ce);
                }
            }
            script.colorsList.RemoveAll((color) => deletable.Any((ce) => ce.hueColorKey == color.hueColorKey));
        }
    }
#endif

}


