﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "HueColorList", menuName = "Explorer/ColorCatcher/HueColorList", order = 0)]
public class HueColorsList : ScriptableObject {

    [Range(0.5f, 1f)]
    public float whiteThreshold = 0.6f;
    [Range(0f, 0.5f)]
    public float blackThreshold = 0.05f;
    [Range(0f, 0.5f)]
    public float greyThreshold = 0.1f;

    public HueColorContainer[] knownColors;

    public List<string> GetMatchingHSLColor(HSLColor hsl){
        List<string> results = new List<string>();
        foreach(HueColorContainer color in knownColors){
            if(color.compareWith(hsl)){
                results.Add(color.Name);
            }
        }
        return results;
    }

}
