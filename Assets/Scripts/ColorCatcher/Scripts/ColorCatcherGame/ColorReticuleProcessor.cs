﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorReticuleProcessor : MonoBehaviour
{
    public HueColorsList colorsList;
    public Image reticule;

    [SerializeField]
    private bool showDebugUI = false;

    public Color pixelColor = new Color(0, 0, 0);

    public void ProcessCameraFrame(Color centerPixel)
    {
        pixelColor = centerPixel;
        reticule.color = pixelColor;
        /*if (showDebugUI){
            reticule.color = ColorUtils.ColorFromHue(ColorUtils.HSLfromColor(pixelColor));
        }else{
            reticule.color = pixelColor;
        }*/
    }

    public MeshRenderer camBackgroundRendererer;
    public Texture cameraTex;
    public Material mat;
    public Texture2D convertTex;
    public UnityEngine.UI.Image colorPicker;

    // Update is called once per frame
    void Update()
    {
        if (!mat)
        {
            mat = GameObject.FindObjectOfType<Vuforia.BackgroundPlaneBehaviour>().Material;
            //camBackgroundRendererer = gameObject.GetComponentInChildren<MeshRenderer>();
        }
        else if (!cameraTex)
        {
            cameraTex = mat.mainTexture;
        }
        else
        {
            convertTex = (Texture2D)cameraTex;
            if (convertTex.width > 0 && convertTex.height > 0)
                ProcessCameraFrame(convertTex.GetPixel(cameraTex.width / 2, cameraTex.height / 2));
        }
    }

    private void OnGUI()
    {
        if(!showDebugUI){
            return;
        }
        var result = "\n\n[ ";
        foreach(string name in GetAimedColor()){
            result += name + ", ";
        }

        var style = new GUIStyle();
        style.fontSize = 70;

        result += " ]";
        GUILayout.Label(result, style);
        GUILayout.Label(ColorUtils.HSLfromColor(pixelColor).ToString(), style);

        GUILayout.Label("White: "+colorsList.whiteThreshold.ToString());
        colorsList.whiteThreshold = GUILayout.HorizontalSlider(colorsList.whiteThreshold, 0, 1f);

        GUILayout.Label("Black: " +colorsList.blackThreshold.ToString());
        colorsList.blackThreshold = GUILayout.HorizontalSlider(colorsList.blackThreshold, 0, 1f);

        GUILayout.Label("Grey: " +colorsList.greyThreshold.ToString());
        colorsList.greyThreshold = GUILayout.HorizontalSlider(colorsList.greyThreshold, 0, 1f);

        if (result.Contains("Blue"))
        {
            Debug.Log("Blue detected");
        }
    }

    public List<string> GetAimedColor(){
        return HSLColorComparator.GetMatchingColorName(colorsList, pixelColor);
    }

}
