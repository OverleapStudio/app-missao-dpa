﻿using UnityEngine;
using UnityEngine.UI;

public class AimColorChange : MonoBehaviour {

    public ColorReticuleProcessor colorReticuleProcessor;
    public Image aimImage;
    public Transform samplingTransform;

    private void Update()
    {
        aimImage.color = colorReticuleProcessor.pixelColor;
    }

}
