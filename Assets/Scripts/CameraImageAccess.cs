using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.UI;

public class CameraImageAccess : MonoBehaviour
{
    public MeshRenderer camBackgroundRendererer;
    public Texture cameraTex;
    public Material mat;
    public Texture2D convertTex;
    public UnityEngine.UI.Image colorPicker;

    // Update is called once per frame
    void Update()
    {
        if (!mat)
        {
            mat = GameObject.FindObjectOfType<Vuforia.BackgroundPlaneBehaviour>().Material;
            //camBackgroundRendererer = gameObject.GetComponentInChildren<MeshRenderer>();
        }
        else if(!cameraTex)
        {
            cameraTex = mat.mainTexture;
        }
        else
        {
            convertTex = (Texture2D)cameraTex;

            Color color = convertTex.GetPixel(cameraTex.width/2, cameraTex.height/2);
            colorPicker.color = color;
        }
    }
}
