﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class ColorMinigame : MinigameBase
{
    [Header("Color Properties")]
    public HueColorsList colorsList;
    public Image reticule;
    public Image captureFill;

    [Header("Intro Properties")]
    public TextMeshProUGUI intro;
    public TextMeshProUGUI introColor;
    public Image introColorBackground;

    [Header("Timer Properties")]
    public GameObject progression;
    public Image progressionFill;
    public TextMeshProUGUI timer;
    public Image challengeColorBackground;
    public TextMeshProUGUI challengeColor;

    [SerializeField]
    private bool captureButton = false;

    public Color pixelColor = new Color(0, 0, 0);

    public override void OnChallengeStart()
    {
        ARMinigameManager.Instance.SetMageCanvas(false);
        base.OnChallengeStart();
        StartCoroutine("TimerMechanic");
        Debug.Log("Challenge Starting");
    }

    public override void OnChallengeIntro()
    {
        base.OnChallengeIntro();
        string[] temp = minigameData._texts.special.Split('|');
        intro.text = temp[0];
        introColor.text = temp[1];
        challengeColor.text = temp[1];
        challengeColorBackground.color = minigameData._color;
        introColorBackground.color = minigameData._color;
    }

    public void ProcessCameraFrame(Color centerPixel)
    {
        pixelColor = centerPixel;
        reticule.color = pixelColor;
        /*if (showDebugUI){
            reticule.color = ColorUtils.ColorFromHue(ColorUtils.HSLfromColor(pixelColor));
        }else{
            reticule.color = pixelColor;
        }*/
    }

    [Header("Color Game")]
    public MeshRenderer camBackgroundRendererer;
    public Texture cameraTex;
    public Material mat = null;
    public Texture2D convertTex;
    public Image colorPicker;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!mat)
        {
            mat = FindObjectOfType<Vuforia.BackgroundPlaneBehaviour>().Material;
        }
        else if (!cameraTex)
        {
            cameraTex = mat.mainTexture;
        }
        else
        {
            convertTex = (Texture2D)mat.mainTexture;
            if (convertTex.width > 0 && convertTex.height > 0)
                ProcessCameraFrame(convertTex.GetPixel(cameraTex.width / 3, cameraTex.height / 3));
        }

        if (captureButton)
        {
            //SIMULATION
            if (reticule.IsActive())
            {
                captureFill.fillAmount -= 0.005f;

                if (captureFill.fillAmount <= 0)
                {
                    OnChallengeComplete();
                    ARMinigameManager.Instance.SetMagicOrb(false);
                }
            }
        }
        else
        {
            if (captureFill.fillAmount <= 1)
            {
                captureFill.fillAmount += 0.03f;
            }
            else if (captureFill.fillAmount <= 0)
            {
                OnChallengeError();
            }
        }


        /*
        if (captureButton)
        {
            if (reticule.IsActive())
            {
                var result = "\n\n[ ";
                foreach (string name in GetAimedColor())
                {
                    result += name + ", ";
                }

                if (result.Contains("Blue"))
                {
                    Debug.Log("Blue detected");
                    captureFill.fillAmount -= 0.01f;

                    if (captureFill.fillAmount <= 0)
                    {
                        OnChallengeComplete();
                    }
                }
                else
                {
                    if (captureFill.fillAmount <= 1)
                    {
                        captureFill.fillAmount += 0.03f;
                    }
                    //OnChallengeError();
                }
            }

            #if UNITY_EDITOR
            if (reticule.IsActive())
            {
                var result = "\n\n[ ";
                foreach (string name in GetAimedColor())
                {
                    result += name + ", ";
                }

                if (result.Contains("Blue"))
                {
                    Debug.Log("Blue detected");
                    captureFill.fillAmount -= 0.01f;

                    if (captureFill.fillAmount <= 0)
                    {
                        OnChallengeComplete();
                    }
                }
                else
                {
                    if (captureFill.fillAmount <= 1)
                    {
                        captureFill.fillAmount += 0.03f;
                    }
                    //OnChallengeError();
                }
            }
            #endif
        }
        else if (captureFill.fillAmount <= 1)
        {
            captureFill.fillAmount += 0.03f;
        }*/

    }

    //TIMER
    public IEnumerator TimerMechanic()
    {
        //MUDAR 110 PARA TEMPO DA MUSICA
        progression.SetActive(true);
        float timeCounter = 60;
        float minutes;
        float seconds;
        string minutesText = "";
        string secondsText = "";

        while (timeCounter >= 0)
        {
            minutes = Mathf.Floor(timeCounter / 60);
            seconds = Mathf.RoundToInt(timeCounter % 60);
            minutesText = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
            secondsText = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
            timer.text = string.Format("{0}:{1}", minutesText, secondsText);
            progressionFill.fillAmount = timeCounter / 60;
            timeCounter--;
            yield return new WaitForSeconds(1.0f);
        }

        captureButton = false;
        OnChallengeError();
        yield return null;
    }

    /*private void OnGUI()
    {
        if (!showDebugUI)
        {
            return;
        }
        var result = "\n\n[ ";
        foreach (string name in GetAimedColor())
        {
            result += name + ", ";
        }

        var style = new GUIStyle();
        style.fontSize = 70;

        result += " ]";
        GUILayout.Label(result, style);
        GUILayout.Label(ColorUtils.HSLfromColor(pixelColor).ToString(), style);

        GUILayout.Label("White: " + colorsList.whiteThreshold.ToString());
        colorsList.whiteThreshold = GUILayout.HorizontalSlider(colorsList.whiteThreshold, 0, 1f);

        GUILayout.Label("Black: " + colorsList.blackThreshold.ToString());
        colorsList.blackThreshold = GUILayout.HorizontalSlider(colorsList.blackThreshold, 0, 1f);

        GUILayout.Label("Grey: " + colorsList.greyThreshold.ToString());
        colorsList.greyThreshold = GUILayout.HorizontalSlider(colorsList.greyThreshold, 0, 1f);

        if (result.Contains("Blue"))
        {
            Debug.Log("Blue detected");
        }
    }*/

    public List<string> GetAimedColor()
    {
        return HSLColorComparator.GetMatchingColorName(colorsList, pixelColor);
    }

    public void CaptureButtonDown()
    {
        captureButton = true;
        ARMinigameManager.Instance.SetMagicOrb(true);
    }

    public void CaptureButtonUp()
    {
        captureButton = false;
        ARMinigameManager.Instance.SetMagicOrb(false);
    }
}