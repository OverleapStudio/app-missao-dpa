using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Minigame", menuName = "Overleap/Minigame", order = 0)]
public class Minigame : ScriptableObject
{
    public string witchName;
    public ulong _vumark = 0;
    public string _resource;
    public MinigameVoice _voices;
    public MinigameTexts _texts;
    public bool hasCamera = false;
    public Color _color;
    public bool isFemale;
}

[Serializable]
public class MinigameVoice
{
    public AudioClip challenge;
    public AudioClip success;
    public AudioClip failure;
}

[Serializable]
public class MinigameTexts
{
    public string challenge;
    public string success;
    public string failure;
    public string special;
}