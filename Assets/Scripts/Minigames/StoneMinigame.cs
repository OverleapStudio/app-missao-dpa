﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class StoneMinigame : MinigameBase
{
    [Header("AR Properties")]
    public GameObject WitchDialog;
    public GameObject InteractionQuestion;

    private Animator shuffleObject;
    private int hiddenStone = 2;

    public override void OnChallengeStart()
    {
        base.OnChallengeStart();
        openContainer.SetActive(false);
        StartCoroutine("ShuffleMechanic");
        Debug.Log("Challenge Starting");
    }

    IEnumerator ShuffleMechanic()
    {
        shuffleObject.SetBool("start",true);
        yield return new WaitForSeconds(16.0f);
        ARMinigameManager.Instance.onARInteraction += InteractionButton;
        InteractionQuestion.SetActive(true);
    }

    public override void OnOpenAndShow()
    {
        base.OnOpenAndShow();
        shuffleObject = ARMinigameManager.Instance.InstantiateShuffleObject(minigameData._texts.special);
        shuffleObject.SetTrigger("reset");
        ARMinigameManager.Instance.SetARMage(minigameData.isFemale);
    }

    public void DialogButton()
    {
        WitchDialog.SetActive(!WitchDialog.activeSelf);
    }

    public void InteractionButton()
    {
        ARMinigameManager.Instance.onARInteraction -= InteractionButton;
        int objectIndex = ARMinigameManager.Instance.GetARIndex();
        if(objectIndex == hiddenStone)
        {
            OnChallengeComplete();
        }
        else
        {
            OnChallengeError();
        }
    }
}