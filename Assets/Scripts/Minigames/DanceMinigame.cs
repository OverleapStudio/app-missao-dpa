﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class DanceMinigame : MinigameBase
{
    [Header("Dancing Properties")]
    public DanceStepsCollection [] danceOptions;
    public DanceStepsCollection danceSteps;
    public TextMeshProUGUI stepMessage;
    public RuntimeAnimatorController [] animatorOptions;
    public Animator danceAnimator;

    [Header("Timer Properties")]
    public GameObject progression;
    public Image progressionFill;
    public TextMeshProUGUI timer;

    public override void OnChallengeStart()
    {
        base.OnChallengeStart();
        danceSteps = minigameData._texts.special.Equals("1") ? danceOptions[0] : danceOptions[1];
        danceAnimator.runtimeAnimatorController = minigameData._texts.special.Equals("1") ? animatorOptions[0] : animatorOptions[1];
        StartCoroutine("DancingMechanic");
        Debug.Log("Challenge Starting");
    }

    //TIMER (TODO: COLOCAR ISSO EM UMA CLASSE SEPARADA)
    public IEnumerator TimerMechanic()
    {
        //MUDAR 110 PARA TEMPO DA MUSICA
        progression.SetActive(true);
        float songLength = danceSteps.steps[danceSteps.steps.Length - 1]._audioClip.length;
        float timeCounter = songLength;
        float minutes;
        float seconds;
        string minutesText = "";
        string secondsText = "";

        while (timeCounter >= 0)
        {
            minutes = Mathf.Floor(timeCounter / 60);
            seconds = Mathf.RoundToInt(timeCounter % 60);
            minutesText = minutes < 10? "0" + minutes.ToString() : minutes.ToString();
            secondsText = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
            timer.text = string.Format("{0}:{1}", minutesText, secondsText);
            progressionFill.fillAmount = timeCounter / songLength;
            timeCounter--;
            yield return new WaitForSeconds(1.0f);
        }
        danceAnimator.SetInteger("Step", 0);

        yield return null;
    }

    //MECANICA DE DANÇA
    public IEnumerator DancingMechanic()
    {
        progression.SetActive(false);
        int stepIndex = 0;
        int messageIndex = 0;

        Debug.Log("Initiate dancing coroutine");
        if (danceSteps==null) yield return null;

        DanceSteps currentStep;

        //Percorre os passos de dança e as mensagens
        while (stepIndex < danceSteps.steps.Length)
        {
            Debug.Log("Initiate dancing loop "+stepIndex);
            currentStep = danceSteps.steps[stepIndex];
            AudioManager.Instance.PlaySoundtrack(currentStep._audioClip, !(stepIndex == danceSteps.steps.Length - 1));
            danceAnimator.SetInteger("Step", currentStep._animationInt);
            danceAnimator.speed = currentStep._speed;

            Debug.Log("Initiate dancing messages " + messageIndex);
            while (messageIndex < currentStep._messages.Length)
            {
                stepMessage.text = currentStep._messages[messageIndex];
                yield return new WaitForSeconds(currentStep._interval);
                messageIndex++;
            }
            messageIndex = 0;
            stepIndex++;

            if(stepIndex == danceSteps.steps.Length - 1)
            {
                StartCoroutine("TimerMechanic");
            }
        }

        //yield return new WaitForSeconds(currentStep._interval);

        danceAnimator.SetInteger("Step", 0);
        AudioManager.Instance.StopSoundtrack();
        yield return new WaitForSeconds(3.0f);
        OnChallengeComplete();
        yield return null;
    }
}