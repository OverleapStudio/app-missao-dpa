﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class SingMinigame : MinigameBase
{
    public override void OnChallengeStart()
    {
        base.OnChallengeStart();
        StartCoroutine("VideoMechanic");
        Debug.Log("Challenge Starting");
    }

    public IEnumerator VideoMechanic()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        while (Screen.currentResolution.height > Screen.currentResolution.width)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        Handheld.PlayFullScreenMovie("ThemeSong.mov", Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForSeconds(0.5f);
        Screen.orientation = ScreenOrientation.Portrait;
        while (Screen.currentResolution.height < Screen.currentResolution.width)
        {
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        OnChallengeComplete();
    }
}