﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;

public class MemoryGame : MinigameBase
{
    [Header("Game Key Settings")]
    public List<Animator> keyLights;
    public List<AudioClip> keySounds;
    public AudioClip keyFailure;
    public GameObject doorUnlock;
    public GameObject doorLock;
    public Image overlay;

    [Header("Gameplay Keys")]
    [SerializeField]
    private List<int> challengeKeys;

    private bool isPlayable = false;
    [SerializeField]
    private int currentKey = 0;
    [SerializeField]
    private int keyLimit = 8;

    public void PlayChallenge()
    {
        isPlayable = false;
        StartCoroutine(ChallengeRoutine());
    }

    IEnumerator ChallengeRoutine()
    {
        yield return new WaitForSeconds(2.0f);
        /*for(int i=0;i<challengeKeys.Count; i++)
        {
            AudioManager.Instance.PlaySFX(keySounds[challengeKeys[i]]);
            keyLights[challengeKeys[i]].SetTrigger("single");
            yield return new WaitForSeconds(1.0f);
        }*/
        foreach(int k in challengeKeys)
        {
            AudioManager.Instance.PlaySFX(keySounds[k]);
            keyLights[k].SetTrigger("single");
            yield return new WaitForSeconds(1.0f);
        }

        currentKey = 0;
        isPlayable = true;
        yield return null;
    }

    public void SortKey(int keyAmount)
    {
        for(int i = 0; i < keyAmount; i++)
        {
            challengeKeys.Add(UnityEngine.Random.Range(0, 4));
        }
    }

    public void OnKeyButton(int keyIndex)
    {
        if (!isPlayable) return;

        AudioManager.Instance.PlaySFX(keySounds[keyIndex]);
        keyLights[keyIndex].SetTrigger("single");

        if (keyIndex == challengeKeys[currentKey])
        {
            currentKey++;
            if (currentKey > keyLimit)
            {
                isPlayable = false;
                StartCoroutine(SuccessRoutine());
            }
            else if(currentKey >= challengeKeys.Count)
            {
                isPlayable = false;
                SortKey(1);
                PlayChallenge();
            }
        }
        else
        {
            isPlayable = false;
            StartCoroutine(FailureRoutine());
        }
    }

    IEnumerator SuccessRoutine()
    {
        yield return new WaitForSeconds(2f);
        keyLights[0].SetTrigger("triple");
        yield return null;
        keyLights[1].SetTrigger("triple");
        yield return null;
        keyLights[2].SetTrigger("triple");
        yield return null;
        keyLights[3].SetTrigger("triple");
        yield return new WaitForSeconds(2.0f);
        overlay.CrossFadeAlpha(0, 1.0f, true);
        yield return new WaitForSeconds(1.0f);
        doorUnlock.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        OnChallengeComplete();
    }

    IEnumerator FailureRoutine()
    {
        AudioManager.Instance.PlaySFX(keyFailure);
        keyLights[challengeKeys[currentKey]].SetTrigger("triple");
        yield return new WaitForSeconds(2.0f);
        overlay.CrossFadeAlpha(0, 1.0f, true);
        yield return new WaitForSeconds(1.0f);
        doorLock.SetActive(true);
        yield return new WaitForSeconds(2.0f);
        OnChallengeError();
    }


    public override void OnChallengeStart()
    {
        base.OnChallengeStart();
        challengeKeys = new List<int>();
        SortKey(1);
        keyLimit = Int16.Parse(minigameData._texts.special);
        PlayChallenge();
    }
}