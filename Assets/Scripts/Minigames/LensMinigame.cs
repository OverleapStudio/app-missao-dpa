﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class LensMinigame : Singleton<LensMinigame>
{
    public AudioManager audioSource;

    [Header("LensGame Panels")]
    public GameObject openContainer1;
    public GameObject openContainer2;
    public GameObject introContainer;
    public GameObject challengeContainer;
    public GameObject successContainer;
    public GameObject scoreContainer;

    [Header("Dialog Objects")]
    public TextMeshProUGUI entryText1;
    public TextMeshProUGUI entryText2;
    public TextMeshProUGUI scoreText;
    public ScrollRect entryScroll1;
    public ScrollRect entryScroll2;
    public ScrollRect scoreScroll;

    [Header("Intro Properties")]
    public TextMeshProUGUI introText;
    public TextMeshProUGUI introColorText;
    public TextMeshProUGUI introColorText1;
    public TextMeshProUGUI introColorText2;
    public Image introColor;
    public Image introColor1;
    public Image introColor2;
    public GameObject uniqueIntroColor;
    public GameObject doubleIntroColor;

    [Header("Color Properties")]
    public HueColorsList colorsList;
    public Image reticule;
    public Image captureFill;

    [Header("Challenge Properties")]
    public GameObject progression;
    public Image progressionFill;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI challengeColorText;
    public TextMeshProUGUI challengeColorText1;
    public TextMeshProUGUI challengeColorText2;
    public Image challengeColor;
    public Image challengeColor1;
    public Image challengeColor2;
    public GameObject uniqueChallengeColor;
    public GameObject doubleChallengeColor;
    public GameObject magicOrb;
    public GameObject magicStone;
    public AudioClip stoneSFX;

    [Header("Score Properties")]
    public List<Image> magicStoneIcons;
    public GameObject winCharacter;
    public GameObject loseCharacter;

    [Header("Main Properties")]
    public AudioPlayerIcons audioIcons;
    public LensConfig lensgameData;
    private Stack<LensChallenge> easyChallenges;
    private Stack<LensChallenge> mediumChallenges;
    private Stack<LensChallenge> hardChallenges;
    private LensChallenge currentChallenge;
    private int stoneCount;
    private int maxStoneCount;

    [SerializeField]
    private bool captureButton = false;

    public Color pixelColor = new Color(0, 0, 0);

    public void Start()
    {
        SetupChallenges();
        var vuforia = Vuforia.VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
        OnChallengeOpen1();
        //Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void SetupChallenges()
    {
        stoneCount = 0;
        maxStoneCount = lensgameData.stoneCount;
        easyChallenges = new Stack<LensChallenge>();
        mediumChallenges = new Stack<LensChallenge>();
        hardChallenges = new Stack<LensChallenge>();

        foreach (LensChallenge challenge in lensgameData.challenges)
        {
            switch (challenge.difficulty)
            {
                case ChallengeDifficulty.easy:
                    easyChallenges.Push(challenge);
                    break;
                case ChallengeDifficulty.medium:
                    mediumChallenges.Push(challenge);
                    break;
                case ChallengeDifficulty.hard:
                    hardChallenges.Push(challenge);
                    break;
            }
        }

        //TODO: SORT CHALLENGES
    }

    private void OnVuforiaStarted()
    {
        Vuforia.CameraDevice.Instance.SetFocusMode(
        Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPaused(bool paused)
    {
        // Set again autofocus mode when app is resumed
        Vuforia.CameraDevice.Instance.SetFocusMode(
        Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
    #region MINIGAME BASE EVENTS
    
    public void OnChallengeOpen1()
    {
        //ARMinigameManager.Instance.SetMageCanvas(false);
        //StartCoroutine("TimerMechanic");
        //Debug.Log("Challenge Starting");
        if (!audioSource) audioSource = AudioManager.Instance;
        entryText1.text = lensgameData.open1.text;
        audioSource.PlayVoice(lensgameData.open1.voice);
        audioSource.PlaySoundtrack(lensgameData.open1Soundtrack);
        openContainer1.SetActive(true);
        StartCoroutine(ScrollAutoRoll(lensgameData.open1.voice.length, entryScroll1, 0.004f)) ;
    }

    public void OnChallengeOpen2()
    {
        openContainer1.SetActive(false);
        entryText2.text = lensgameData.open2.text;
        audioSource.PlayVoice(lensgameData.open2.voice);
        audioSource.PlaySoundtrack(lensgameData.open2Soundtrack);
        openContainer2.SetActive(true);
        StartCoroutine(ScrollAutoRoll(lensgameData.open2.voice.length, entryScroll2, 0.003f));
    }

    public void OnChallengeIntro()
    {
        //Reset color capture variables
        captureFill.fillAmount = 1;
        captureButton = false;
        magicStone.SetActive(false);
        audioSource.StopVoice();

        if (stoneCount <= 0)
        {
            audioSource.PlaySoundtrack(lensgameData.introSoundtrack);
            AudioManager.Instance.SetSoundtrackVolume(0.3f);
            openContainer2.SetActive(false);
        }

        successContainer.SetActive(false);
        challengeContainer.SetActive(false);
        introContainer.SetActive(true);

        if (stoneCount < 9)
        {
            currentChallenge = easyChallenges.Pop();
        }
        else if(stoneCount < 12)
        {
            currentChallenge = mediumChallenges.Pop();
        }
        else
        {
            currentChallenge = hardChallenges.Pop();
        }

        if(string.IsNullOrEmpty(currentChallenge.colorName2))
        {
            introText.text = "Sua missão é encontrar um objeto da cor";
            challengeColorText.text = introColorText.text = currentChallenge.colorName1;
            challengeColor.color = introColor.color = currentChallenge.color1;
            
            uniqueIntroColor.SetActive(true);
            uniqueChallengeColor.SetActive(true);
            doubleIntroColor.SetActive(false);
            doubleChallengeColor.SetActive(false);

            if (currentChallenge.isColorDark)
            {
                introColorText.color = challengeColorText.color = Color.white;
            }
            else
            {
                introColorText.color = challengeColorText.color = Color.black;
            }
        }
        else
        {
            introText.text = "Sua missão é encontrar um objeto da mistura das cores";
            challengeColor1.color = introColor1.color = currentChallenge.color1;
            challengeColor2.color = introColor2.color = currentChallenge.color2;
            challengeColorText1.text = introColorText1.text = currentChallenge.colorName1;
            challengeColorText2.text = introColorText2.text = currentChallenge.colorName2;

            uniqueIntroColor.SetActive(false);
            uniqueChallengeColor.SetActive(false);
            doubleIntroColor.SetActive(true);
            doubleChallengeColor.SetActive(true);

            if (currentChallenge.isColorDark)
            {
                introColorText1.color = introColorText2.color = challengeColorText2.color = challengeColorText1.color = Color.white;
            }
            else
            {
                introColorText1.color = introColorText2.color = challengeColorText2.color = challengeColorText1.color = Color.black;
            }
        }

        audioSource.PlayVoice(currentChallenge.introDialog.voice);
        //string[] temp = minigameData._texts.special.Split('|');
        //intro.text = temp[0];
        //introColor.text = temp[1];
        //challengeColor.text = temp[1];
        //challengeColorBackground.color = minigameData._color;
        //introColorBackground.color = minigameData._color;
    }

    public void OnChallengeStart()
    {
        if(stoneCount <= 0)
        {
            audioSource.PlaySoundtrack(lensgameData.challengeSoundtrack, true);
            AudioManager.Instance.SetSoundtrackVolume(0.4f);
        }
        introContainer.SetActive(false);
        challengeContainer.SetActive(true);

        StartCoroutine("TimerMechanic");
    }

    void OnChallengeComplete()
    {
        StopAllCoroutines();
        stoneCount++;
        if (stoneCount >= maxStoneCount)
        {
            OnChallengeScore();
            return;
        }

        magicStone.SetActive(true);
        challengeContainer.SetActive(false);
        successContainer.SetActive(true);
        AudioManager.Instance.PlaySFX(stoneSFX);
        audioSource.PlayVoice(lensgameData.success[Random.Range(0,lensgameData.success.Count-1)].voice);
        
    }

    void OnChallengeScore()
    {
        StopAllCoroutines();
        challengeContainer.SetActive(false);
        successContainer.SetActive(false);
        scoreContainer.SetActive(true);

        ScoreDialog finalScore = new ScoreDialog();

        for(int i = 0; i < 4; i++)
        {
            if(stoneCount <= lensgameData.score[i].stoneCount)
            {
                finalScore = lensgameData.score[i];
                i = 4;
            }
        }

        Color solidColor = new Color(1, 1, 1, 1);
        Color transparentColor = new Color(1, 1, 1, 0.4f);

        for (int i = 0; i < magicStoneIcons.Count; i++)
        {
            if (i < stoneCount)
            {
                magicStoneIcons[i].color = solidColor;
            }
            else
            {
                magicStoneIcons[i].color = transparentColor;
            }
        }

        if(stoneCount < maxStoneCount)
        {
            winCharacter.SetActive(false);
            loseCharacter.SetActive(true);
            audioSource.PlaySoundtrack(lensgameData.loseSoundtrack);
        }
        else
        {
            winCharacter.SetActive(true);
            loseCharacter.SetActive(false);
            audioSource.PlaySoundtrack(lensgameData.scoreSoundtrack);
        }

        scoreText.text = finalScore.text;
        audioSource.PlayVoice(finalScore.voice);
        StartCoroutine(ScrollAutoRoll(finalScore.voice.length, scoreScroll, 0.007f));
    }

    //PLAY - PAUSE - REPEAT Voice Dialog
    public virtual void PlayChallengeButton(Image playButton)
    {
        if (playButton.sprite == audioIcons.play)
        {
            audioSource.PlayVoice();
            playButton.sprite = audioIcons.pause;
        }
        else if (playButton.sprite == audioIcons.pause)
        {
            audioSource.PauseVoice();
            playButton.sprite = audioIcons.play;
        }
        else
        {
            audioSource.RestartVoice();
            playButton.sprite = audioIcons.play;
        }
    }

    //DIALOG AUTO ROLL
    public IEnumerator ScrollAutoRoll(float duration, ScrollRect dialogScroll,float speed)
    {
        //Debug.Log("Starting auto roll");
        dialogScroll.verticalNormalizedPosition = 1;
        yield return new WaitForSeconds(4f);
        while (dialogScroll.verticalNormalizedPosition >= 0)
        {
            //Debug.Log("Rolling");
            yield return new WaitForSeconds(duration / 1000);
            dialogScroll.verticalNormalizedPosition -= (speed/2);
        }
        yield return null;
    }
    #endregion

    public void ProcessCameraFrame(Color centerPixel)
    {
        pixelColor = centerPixel;
        reticule.color = pixelColor;
        /*if (showDebugUI){
            reticule.color = ColorUtils.ColorFromHue(ColorUtils.HSLfromColor(pixelColor));
        }else{
            reticule.color = pixelColor;
        }*/
    }

    [Header("Color Game")]
    public MeshRenderer camBackgroundRendererer;
    public Texture cameraTex;
    public Material mat = null;
    public Texture2D convertTex;
    public Image colorPicker;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!mat)
        {
            mat = FindObjectOfType<Vuforia.BackgroundPlaneBehaviour>().Material;
        }
        else if (!cameraTex)
        {
            cameraTex = mat.mainTexture;
        }
        else
        {
            convertTex = (Texture2D)mat.mainTexture;
            if (convertTex.width > 0 && convertTex.height > 0)
                ProcessCameraFrame(convertTex.GetPixel(cameraTex.width / 3, cameraTex.height / 3));
        }

        if (captureButton)
        {
            //SIMULATION
            if (reticule.IsActive())
            {
                captureFill.fillAmount -= 0.005f;

                if (captureFill.fillAmount <= 0)
                {
                    OnChallengeComplete();
                    magicOrb.SetActive(false);
                }
            }
        }
        else
        {
            if (captureFill.fillAmount <= 1)
            {
                captureFill.fillAmount += 0.03f;
            }
            else if (captureFill.fillAmount <= 0)
            {
                OnChallengeScore();
            }
        }


        /*
        if (captureButton)
        {
            if (reticule.IsActive())
            {
                var result = "\n\n[ ";
                foreach (string name in GetAimedColor())
                {
                    result += name + ", ";
                }

                if (result.Contains("Blue"))
                {
                    Debug.Log("Blue detected");
                    captureFill.fillAmount -= 0.01f;

                    if (captureFill.fillAmount <= 0)
                    {
                        OnChallengeComplete();
                    }
                }
                else
                {
                    if (captureFill.fillAmount <= 1)
                    {
                        captureFill.fillAmount += 0.03f;
                    }
                    //OnChallengeError();
                }
            }

            #if UNITY_EDITOR
            if (reticule.IsActive())
            {
                var result = "\n\n[ ";
                foreach (string name in GetAimedColor())
                {
                    result += name + ", ";
                }

                if (result.Contains("Blue"))
                {
                    Debug.Log("Blue detected");
                    captureFill.fillAmount -= 0.01f;

                    if (captureFill.fillAmount <= 0)
                    {
                        OnChallengeComplete();
                    }
                }
                else
                {
                    if (captureFill.fillAmount <= 1)
                    {
                        captureFill.fillAmount += 0.03f;
                    }
                    //OnChallengeError();
                }
            }
            #endif
        }
        else if (captureFill.fillAmount <= 1)
        {
            captureFill.fillAmount += 0.03f;
        }*/

    }

    //TIMER
    public IEnumerator TimerMechanic()
    {
        //MUDAR 110 PARA TEMPO DA MUSICA
        progression.SetActive(true);
        float timeCounter = currentChallenge.timeLimit;
        float timeLimit = currentChallenge.timeLimit;
        float minutes;
        float seconds;
        string minutesText = "";
        string secondsText = "";

        while (timeCounter >= 0)
        {
            minutes = Mathf.Floor(timeCounter / 60);
            seconds = Mathf.RoundToInt(timeCounter % 60);
            minutesText = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
            secondsText = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
            timer.text = string.Format("{0}:{1}", minutesText, secondsText);
            progressionFill.fillAmount = timeCounter / timeLimit;
            timeCounter--;
            yield return new WaitForSeconds(1.0f);
        }

        captureButton = false;
        OnChallengeScore();
        yield return null;
    }

    /*private void OnGUI()
    {
        if (!showDebugUI)
        {
            return;
        }
        var result = "\n\n[ ";
        foreach (string name in GetAimedColor())
        {
            result += name + ", ";
        }

        var style = new GUIStyle();
        style.fontSize = 70;

        result += " ]";
        GUILayout.Label(result, style);
        GUILayout.Label(ColorUtils.HSLfromColor(pixelColor).ToString(), style);

        GUILayout.Label("White: " + colorsList.whiteThreshold.ToString());
        colorsList.whiteThreshold = GUILayout.HorizontalSlider(colorsList.whiteThreshold, 0, 1f);

        GUILayout.Label("Black: " + colorsList.blackThreshold.ToString());
        colorsList.blackThreshold = GUILayout.HorizontalSlider(colorsList.blackThreshold, 0, 1f);

        GUILayout.Label("Grey: " + colorsList.greyThreshold.ToString());
        colorsList.greyThreshold = GUILayout.HorizontalSlider(colorsList.greyThreshold, 0, 1f);

        if (result.Contains("Blue"))
        {
            Debug.Log("Blue detected");
        }
    }*/

    public List<string> GetAimedColor()
    {
        return HSLColorComparator.GetMatchingColorName(colorsList, pixelColor);
    }

    public void CaptureButtonDown()
    {
        captureButton = true;
        magicOrb.SetActive(true);
    }

    public void CaptureButtonUp()
    {
        captureButton = false;
        magicOrb.SetActive(false);
    }

    public void ExitToMainMenu()
    {
        LoaderManager.Instance.OnHiddenLoadingRequest(0);
    }

    public void ResetScene()
    {
        LoaderManager.Instance.OnHiddenLoadingRequest(3);
    }
}