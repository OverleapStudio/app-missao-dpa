using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StepsCollection", menuName = "Overleap/DanceMinigame/StepsCollection", order = 0)]
public class DanceStepsCollection : ScriptableObject
{
    [SerializeField]
    public DanceSteps[] steps;
}

[System.Serializable]
public class DanceSteps
{
    public int _animationInt;
    public AudioClip _audioClip;
    public string[] _messages;
    [Range(0f, 1f)]
    public float _speed;
    public float _duration;
    public float _interval;
}