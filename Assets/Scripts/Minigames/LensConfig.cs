using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "LensConfig", menuName = "Overleap/LensMinigame", order = 0)]
public class LensConfig : ScriptableObject
{
    public int stoneCount;
    public List<LensChallenge> challenges;
    public Dialog open1;
    public Dialog open2;
    public List<Dialog> success;
    public List<ScoreDialog> score;
    public AudioClip open1Soundtrack;
    public AudioClip open2Soundtrack;
    public AudioClip introSoundtrack;
    public AudioClip challengeSoundtrack;
    public AudioClip scoreSoundtrack;
    public AudioClip loseSoundtrack;
}

[Serializable]
public struct LensChallenge
{
    public Dialog introDialog;
    public ChallengeDifficulty difficulty;
    public float timeLimit;
    public Color color1;
    public Color color2;
    public string colorName1;
    public string colorName2;
    public bool isColorDark;
}

[Serializable]
public struct Dialog
{
    public AudioClip voice;
    public string text;
}

[Serializable]
public struct ScoreDialog
{
    public AudioClip voice;
    public string text;
    public int stoneCount;
}

[Serializable]
public enum ChallengeDifficulty
{
    easy,
    medium,
    hard
}

/*
[Serializable]
public class MinigameVoice
{
    public AudioClip challenge;
    public AudioClip success;
    public AudioClip failure;
}

[Serializable]
public class MinigameTexts
{
    public string challenge;
    public string success;
    public string failure;
    public string special;
}*/