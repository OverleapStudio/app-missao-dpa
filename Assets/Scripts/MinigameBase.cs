using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class MinigameBase : MonoBehaviour
{
    [Header("Minigame Base")]
    public AudioManager audioSource;
    public MinigameVoice voices;
    public MinigameSoundtrack soundtracks;
    public GameObject openContainer;
    public GameObject introContainer;
    public GameObject challengeContainer;
    public GameObject successContainer;
    public GameObject failureContainer;
    public TextMeshProUGUI entryText;
    public TextMeshProUGUI successText;
    public TextMeshProUGUI failureText;
    public TextMeshProUGUI witchName;
    public ScrollRect entryScroll;
    public ScrollRect successScroll;
    public ScrollRect failureScroll;
    public Image entryWitch;
    public Sprite femaleWitch;

    public delegate void OnDestroyChallenge();
    public event OnDestroyChallenge onDestroyChallenge;

    public AudioPlayerIcons audioIcons;
    public Minigame minigameData;

    //STEP 1 - Open Initial Dialog and Play Voice
    public virtual void OnOpenAndShow()
    {
        if (minigameData.isFemale) entryWitch.sprite = femaleWitch;
        if (witchName) witchName.text = minigameData.witchName;
        voices = minigameData._voices;
        entryText.text = minigameData._texts.challenge;
        if (!audioSource) audioSource = AudioManager.Instance;
        audioSource.PlayVoice(voices.challenge);
        audioSource.PlaySoundtrack(soundtracks.open);
        openContainer.SetActive(true);
        StartCoroutine(ScrollAutoRoll(voices.challenge.length, entryScroll));
        //Invoke("OnChallengeStart",minigameAudio.challenge.length);
    }

    //BUTTON TO STEP 2 - Optional Introduction Before Challenge
    public virtual void StartChallengeButton()
    {
        OnChallengeIntro();
    }

    //PLAY - PAUSE - REPEAT Voice Dialog
    public virtual void PlayChallengeButton(Image playButton)
    {
        if (playButton.sprite == audioIcons.play)
        {
            audioSource.PlayVoice();
            playButton.sprite = audioIcons.pause;
        }
        else if (playButton.sprite == audioIcons.pause)
        {
            audioSource.PauseVoice();
            playButton.sprite = audioIcons.play;
        }
        else
        {
            audioSource.RestartVoice();
            playButton.sprite = audioIcons.play;
        }
    }

    //STEP 2 - Optional Introduction Before Challenge
    public virtual void OnChallengeIntro()
    {
        audioSource.StopVoice();
        audioSource.PlaySoundtrack(soundtracks.intro);
        AudioManager.Instance.SetSoundtrackVolume(0.3f);
        openContainer.SetActive(false);
        introContainer.SetActive(true);
    }

    //STEP 3 - Challenge Starts
    public virtual void OnChallengeStart()
    {
        audioSource.PlaySoundtrack(soundtracks.game, true);
        AudioManager.Instance.SetSoundtrackVolume(0.5f);
        introContainer.SetActive(false);
        challengeContainer.SetActive(true);
    }

    public virtual void OnChallengeComplete()
    {
        StopAllCoroutines();
        successText.text = minigameData._texts.success;
        audioSource.PlayVoice(voices.success);
        audioSource.PlaySoundtrack(soundtracks.success);
        challengeContainer.SetActive(false);
        successContainer.SetActive(true);
        StartCoroutine(ScrollAutoRoll(voices.success.length, successScroll));
        AudioManager.Instance.SetSoundtrackVolume(0.3f);
    }

    public virtual void OnChallengeError()
    {
        StopAllCoroutines();
        failureText.text = minigameData._texts.failure;
        audioSource.PlayVoice(voices.failure);
        audioSource.PlaySoundtrack(soundtracks.failure);
        challengeContainer.SetActive(false);
        failureContainer.SetActive(true);
        StartCoroutine(ScrollAutoRoll(voices.failure.length, failureScroll));
        AudioManager.Instance.SetSoundtrackVolume(0.3f);
    }

    public virtual void DestroyChallenge()
    {
        if (onDestroyChallenge != null)
        {
            onDestroyChallenge();
        }
        challengeContainer.SetActive(false);
        AudioManager.Instance.StopVoice();
        Destroy(this.gameObject);
    }

    public IEnumerator ScrollAutoRoll(float duration, ScrollRect dialogScroll)
    {
        //Debug.Log("Starting auto roll");
        dialogScroll.verticalNormalizedPosition = 1;
        yield return new WaitForSeconds(4f);
        while (dialogScroll.verticalNormalizedPosition >= 0)
        {
            //Debug.Log("Rolling");
            yield return new WaitForSeconds(duration/500);
            dialogScroll.verticalNormalizedPosition -= 0.005f;
        }
        yield return null;
    }
}

[Serializable]
public class MinigameSoundtrack
{
    public AudioClip open;
    public AudioClip intro;
    public AudioClip game;
    public AudioClip success;
    public AudioClip failure;
}

[Serializable]
public class AudioPlayerIcons
{
    public Sprite play;
    public Sprite pause;
    public Sprite restart;
}