﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadURL : MonoBehaviour
{
    public string mUrl;

    public void OpenURL()
    {
        if (!string.IsNullOrEmpty(mUrl))
        {
            Application.OpenURL(mUrl);
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }
}
