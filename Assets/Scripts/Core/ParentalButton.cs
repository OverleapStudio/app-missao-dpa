using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParentalButton : MonoBehaviour
{
    public int buttonIndex;
    private Animator _animator;
    private Button _button;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _button = GetComponent<Button>();
        _button.interactable = true;
    }

    public void SelectButton()
    {
        if(_animator == null) _animator = GetComponent<Animator>();
        if (_button == null) _button = GetComponent<Button>();
        _animator.SetTrigger("Selected");
        _button.interactable = false;
    }

    public void ResetButton()
    {
        if (_animator == null) _animator = GetComponent<Animator>();
        if (_button == null) _button = GetComponent<Button>();
        _animator.SetTrigger("Normal");
        _button.interactable = true;
    }

    public bool isSelected()
    {
        return !_button.interactable;
    }
}
