﻿/*using UnityEngine;
using System;

public class ProductData : RegularProductsModel
{
    //public RegularProductsModel mainProduct;
    private Sprite imageSprite;
    private int starRating;
    private string textRating;
    public bool availableContent = true;
    public int invoiceIndex = 0;
    public int installment = 0;
    public int bookId;
    public DateTime bookDate;

    public Action<Sprite> OnSpriteChange;

    public ProductData(RegularProductsModel regularProduct)
    {
        InvoicesModel invoice = UserManager.instance.invoicesData.Find(i => i.id == regularProduct.invoiceId);
        installment = invoice.installment;

        try
        {
            bookDate = DateTime.ParseExact(invoice.checkout_date, "yyyy-MM-dd HH:mm:ss", null);
        }
        catch
        {
            try
            {
                bookDate = DateTime.ParseExact(invoice.payment_date, "yyyy-MM-dd HH:mm:ss", null);
            }
            catch
            {
                try
                {
                    bookDate = DateTime.ParseExact(invoice.expire_date, "yyyy-MM-dd HH:mm:ss", null);
                }
                catch
                {
                    try
                    {
                        bookDate = DateTime.ParseExact(invoice.send_date, "yyyy-MM-dd HH:mm:ss", null);
                    }
                    catch
                    {
                        bookDate = DateTime.ParseExact(UserManager.instance.activeSubscriptionData.created_at, "yyyy-MM-dd HH:mm:ss", null);
                        //bookDate = DateTime.ParseExact("yyyy-MM-DD HH:mm:ss", "yyyy-MM-DD HH:mm:ss", null);
                        Debug.LogWarning("INVALID DATE");
                    }
                }
            }
        }

        if (string.IsNullOrEmpty(regularProduct.know_more))
        {
            availableContent = false;
        }

        regularProduct.name = UtilityAPI.ConvertToTitle(regularProduct.name);
        starRating = PlayerPrefs.GetInt(name+ "rating");
        textRating = PlayerPrefs.GetString(name + "text");
        invoiceId = regularProduct.invoiceId;
        UtilityAPI.DebugLog(String.Format("Added product {0} with installment {1}",regularProduct.name,installment));
    }

    public Sprite GetImage()
    {
        if(imageSprite == null)
        {
            imageSprite = Resources.Load<Sprite>("BookPlaceholder");
        }

        return imageSprite;
    }
    public void SetImage(Sprite spr)
    {
        imageSprite = spr;
        OnSpriteChange?.Invoke(spr);
    }

    public string GetTag()
    {
        string tag = "";
        try
        {
            tag = mainProduct.tags[0];
        }
        catch
        {
            //-Debug.Log("Invalid tag");
        }
        return tag;
    }

    public int GetChild()
    {
        return mainProduct.child_id;
    }

    public void SetChild(int index)
    {
        mainProduct.child_id = index;
    }

    public int GetStarRating()
    {
        return starRating;
    }

    public void SetStarRating(int index)
    {
        starRating = index;
    }
    public string GetTextRating()
    {
        return textRating;
    }

    public void SetTextRating(string txt)
    {
        textRating = txt;
    }

    /*
    async void SetSprite()
    {
        if (string.IsNullOrEmpty(mainProduct.image))
        {
            image = Resources.Load<Sprite>("BookPlaceholder");
        }

        try
        {
            if (mainProduct.image.Contains("png") || mainProduct.image.Contains("jpg"))
            {
                Texture2D texture = await UtilityAPI.GetRemoteTexture(mainProduct.image);
                Debug.Log(texture);
                image = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
            }
            else
            {
                image = Resources.Load<Sprite>("BookPlaceholder");
            }
        }
        catch
        {
            image = Resources.Load<Sprite>("BookPlaceholder");
            Debug.LogWarning("Erro ao carregar a imagem do livro.");
        }


        
        RestClient.Request(new RequestHelper
        {
            Uri = mainProduct.image,
            Method = "GET",
            DownloadHandler = new DownloadHandlerTexture(),
            IgnoreHttpException = true,
            ChunkedTransfer = false,
            ParseResponseBody = false,
            DefaultContentType = false
        }).Then(res => {
            Texture2D img = ((DownloadHandlerTexture)res.Request.downloadHandler).texture;

            image = Sprite.Create(img, new Rect(0.0f, 0.0f, img.width, img.height), new Vector2(0.5f, 0.5f), 100.0f);
        }).Catch(err => {
            Debug.Log(err);
            image = null;
        });
    }
}*/