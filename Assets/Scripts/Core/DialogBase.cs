﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogBase : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI titleText;
    [SerializeField] protected TextMeshProUGUI bodyText = null;
    [SerializeField] protected Button affirmativeButton;
    [SerializeField] protected Button negativeButton;
    [SerializeField] protected RectTransform dialogContainer;
    [SerializeField] protected Image blackBackground;
    //[SerializeField] private bool isModal = true;
    [SerializeField] private bool destroyOnHide = true; 
    [SerializeField] private SlideDirection showDirection = SlideDirection.Up;
    [SerializeField] private SlideDirection hideDirection = SlideDirection.Down;

    public Action OnShowStart;
    public Action OnShowEnd;
    public Action OnHideStart;
    public Action OnHideEnd;

    public virtual void InitializeAndShow(string title, string body, UnityAction onAffirmativeButtonClick = null, UnityAction onNegativeButtonClick = null)
    {
        Initialize(title, body, onAffirmativeButtonClick, onNegativeButtonClick);
        Show();
    }

    public virtual void Initialize(string title, string body, UnityAction onAffirmativeButtonClick = null, UnityAction onNegativeButtonClick = null)
    {
        if (titleText && title != null) titleText.text = title;
        if (bodyText && body != null) bodyText.text = body;
        if (affirmativeButton && onAffirmativeButtonClick != null)
        {
            affirmativeButton.onClick.RemoveListener(onAffirmativeButtonClick);
            affirmativeButton.onClick.AddListener(onAffirmativeButtonClick);
        }
        if (negativeButton && onNegativeButtonClick != null)
        {
            negativeButton.onClick.RemoveListener(onNegativeButtonClick);
            negativeButton.onClick.AddListener(onNegativeButtonClick);
        }

        Button bgButton = blackBackground ? blackBackground.GetComponent<Button>() : null;
        if (bgButton != null && onNegativeButtonClick != null) bgButton.onClick.AddListener(onNegativeButtonClick);

        gameObject.SetActive(false);
    }

    public virtual void Show()
    {
        Show(true);
    }

    public virtual void Show(bool anim)
    {
        OnShowStart?.Invoke();

        if (anim)
        {
            dialogContainer.anchoredPosition = GetSlidePosition(GetOpositeDirection(showDirection));

            if (blackBackground)
            {
                Color c = blackBackground.color;
                blackBackground.color = new Color(c.r, c.g, c.b, 0f);
            }

            gameObject.SetActive(true);

            var bgTween = blackBackground ? LeanTween.alpha(blackBackground.GetComponent<RectTransform>(), 0.627451f, 0.5f) : null;
            var tween = LeanTween.move(dialogContainer, dialogContainer.anchoredPosition + GetSlidePosition(showDirection), 0.5f).setEaseOutCubic();

            bool bgTweenComplete = bgTween == null;
            bool windowTweenComplete = false;

            tween.setOnComplete(() =>
            {
                if (bgTweenComplete)
                {
                    OnShowEnd?.Invoke();
                }
                else windowTweenComplete = true;
            });

            if (bgTween != null)
            {
                bgTween.setOnComplete(() =>
                {
                    if (windowTweenComplete)
                    {
                        OnShowEnd?.Invoke();
                    }
                    else bgTweenComplete = true;
                });
            }
        }
        else
        {
            OnShowEnd?.Invoke();
            gameObject.SetActive(true);
        }
    }

    public virtual void Hide()
    {
        Hide(true);
    }

    public virtual void Hide(bool anim)
    {
        OnHideStart?.Invoke();

        if (anim)
        {
            var bgTween = blackBackground ? LeanTween.alpha(blackBackground.GetComponent<RectTransform>(), 0f, 0.5f) : null;
            var tween = LeanTween.move(dialogContainer, GetSlidePosition(hideDirection), 0.5f).setEaseOutCubic();

            bool bgTweenComplete = bgTween == null;
            bool windowTweenComplete = false;

            tween.setOnComplete(() =>
            {
                if (bgTweenComplete)
                {
                    OnHideEnd?.Invoke();
                    if (destroyOnHide) Destroy(gameObject);
                    else gameObject.SetActive(false);
                }
                else windowTweenComplete = true;
            });

            if (bgTween != null)
            {
                bgTween.setOnComplete(() =>
                {
                    if (windowTweenComplete)
                    {
                        OnHideEnd?.Invoke();
                        if (destroyOnHide) Destroy(gameObject);
                        else gameObject.SetActive(false);
                    }
                    else bgTweenComplete = true;
                });
            }
        }
        else
        {
            OnHideEnd?.Invoke();
            gameObject.SetActive(false);
        }
    }

    private SlideDirection GetOpositeDirection(SlideDirection direction)
    {
        switch(direction)
        {
            case SlideDirection.Down: return SlideDirection.Up;
            case SlideDirection.Up: return SlideDirection.Down;
            case SlideDirection.Right: return SlideDirection.Left;
            case SlideDirection.Left: return SlideDirection.Right;
        }

        return SlideDirection.Up;
    }

    private Vector2 GetSlidePosition(SlideDirection direction)
    {
        float canvasSize = (direction == SlideDirection.Left || direction == SlideDirection.Right) ? dialogContainer.GetRootCanvas().pixelRect.width : dialogContainer.GetRootCanvas().pixelRect.height;
        float dialogSize = (direction == SlideDirection.Left || direction == SlideDirection.Right) ? dialogContainer.rect.width : dialogContainer.rect.height;

        dialogSize *= 1.1f;

        switch (direction)
        {
            case SlideDirection.Down:
                return new Vector2(0f, -(canvasSize + dialogSize));
            case SlideDirection.Left:
                return new Vector2(-(canvasSize + dialogSize), 0f);
            case SlideDirection.Right:
                return new Vector2(canvasSize + dialogSize, 0f);
            case SlideDirection.Up:
                return new Vector2(0f, canvasSize + dialogSize);
        }

        return Vector2.zero;
    }
}
