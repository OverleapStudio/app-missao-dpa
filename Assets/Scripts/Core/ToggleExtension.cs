﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ToggleExtension : MonoBehaviour
{
    public RectTransform BulletRect;
    public Toggle mToggle;
    private Vector3 scaleDouble = new Vector3(1.5f, 1.5f, 1.5f);
    private Vector3 scaleNormal = new Vector3(1f, 1f, 1f);

    public void Start()
    {
        OnValueChanged();
    }

    public void OnValueChanged()
    {
        if (mToggle.isOn)
        {
            LeanTween.scale(BulletRect, scaleDouble, 0.2f);
        }
        else
        {
            LeanTween.scale(BulletRect, scaleNormal, 0.2f);
        }
    }
}
