using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationListener : MonoBehaviour
{
    public MinigameBase minigame;

    public void StartEventListener()
    {
        minigame.OnChallengeStart();
    }
}
