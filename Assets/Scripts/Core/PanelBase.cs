﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PanelBase : MonoBehaviour
{
    [SerializeField] private RectTransform dialogContainer = null;
    //[SerializeField] private bool isModal = true;
    [SerializeField] private SlideDirection openDirection = SlideDirection.Up;
    [SerializeField] private SlideDirection closeDirection = SlideDirection.Down;

    public Action<PanelBase> OnShowStart { get; set; }
    public Action<PanelBase> OnShowEnd { get; set; }
    public Action<PanelBase> OnHideStart { get; set; }
    public Action<PanelBase> OnHideEnd { get; set; }

    public virtual void Show()
    {
        Show(null, null);
    }

    public virtual void Show(Action onShowStart, Action onShowEnd)
    {
        Show(true, onShowStart, onShowEnd);
    }

    public virtual void Show(bool anim, Action onShowStart, Action onShowEnd)
    {
        OnShowStart?.Invoke(this);
        onShowStart?.Invoke();

        if (anim)
        {
            dialogContainer.anchoredPosition = GetSlidePosition(GetOpositeDirection(openDirection));
            gameObject.SetActive(true);

            var tween = LeanTween.move(dialogContainer, dialogContainer.anchoredPosition + GetSlidePosition(openDirection), 0.5f).setEaseOutCubic();

            tween.setOnComplete(() =>
            {
                OnShowEnd?.Invoke(this);
                onShowEnd?.Invoke();
                gameObject.SetActive(true);
            });
        }
        else
        {
            OnShowEnd?.Invoke(this);
            onShowEnd?.Invoke();
            gameObject.SetActive(true);
        }
    }

    public virtual void Hide()
    {
        Hide(true);
    }

    public virtual void Hide(bool destroy)
    {
        Hide(destroy, null, null);
    }

    public virtual void Hide(bool destroy, Action onHideStart, Action onHideEnd)
    {
        Hide(true, destroy, onHideStart, onHideEnd);
    }

    public virtual void Hide(bool anim, bool destroy, Action onHideStart, Action onHideEnd)
    {
        OnHideStart?.Invoke(this);
        onHideStart?.Invoke();

        if (anim)
        {
            var tween = LeanTween.move(dialogContainer, GetSlidePosition(closeDirection), 0.5f).setEaseOutQuad();

            tween.setOnComplete(() =>
            {
                OnHideEnd?.Invoke(this);
                onHideEnd?.Invoke();
                if (destroy) Destroy(gameObject);
                else gameObject.SetActive(false);
            });
        }
        else
        {
            OnHideEnd?.Invoke(this);
            onHideEnd?.Invoke();
            gameObject.SetActive(false);
        }
    }

    private SlideDirection GetOpositeDirection(SlideDirection direction)
    {
        switch (direction)
        {
            case SlideDirection.Down: return SlideDirection.Up;
            case SlideDirection.Up: return SlideDirection.Down;
            case SlideDirection.Right: return SlideDirection.Left;
            case SlideDirection.Left: return SlideDirection.Right;
            default:
                break;
        }

        return SlideDirection.Up;
    }

    private Vector2 GetSlidePosition(SlideDirection direction)
    {
        float canvasSize = (direction == SlideDirection.Left || direction == SlideDirection.Right) ? dialogContainer.GetRootCanvas().pixelRect.width : dialogContainer.GetRootCanvas().pixelRect.height;
        float dialogSize = (direction == SlideDirection.Left || direction == SlideDirection.Right) ? dialogContainer.rect.width : dialogContainer.rect.height;

        dialogSize *= 1.1f;

        switch (direction)
        {
            case SlideDirection.Down:
                return new Vector2(0f, -(canvasSize + dialogSize));
            case SlideDirection.Left:
                return new Vector2(-(canvasSize + dialogSize), 0f);
            case SlideDirection.Right:
                return new Vector2(canvasSize + dialogSize, 0f);
            case SlideDirection.Up:
                return new Vector2(0f, canvasSize + dialogSize);
            default:
                break;
        }

        return Vector2.zero;
    }
}
