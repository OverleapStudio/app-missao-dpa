﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DentedPixel;

public enum SlideDirection
{
    Up,
    Right,
    Down,
    Left
}

public class PanelAnimation : MonoBehaviour
{
    private CanvasGroup panelCanvas;
    private RectTransform panelRect;
    private RectTransform parentRect;

    public bool isHome = false;
    public bool animOnEnable = true;
    private bool isVertical = false;

    private RectTransform PanelRect
    {
        get
        {
            if(panelRect == null) panelRect = GetComponent<RectTransform>();
            return panelRect;
        }
        set 
        { 
            panelRect = value; 
        }
    }

    private CanvasGroup PanelCanvas
    {
        get
        {
            if (panelCanvas == null) panelCanvas = GetComponent<CanvasGroup>();
            return panelCanvas;
        }
        set
        {
            panelCanvas = value;
        }
    }

    private RectTransform ParentRect
    {
        get
        {
            if (parentRect == null) parentRect = GetComponentInParent<RectTransform>();
            return parentRect;
        }
        set
        {
            parentRect = value;
        }
    }

    void Awake()
    {
        if(PanelCanvas == null)
        {
            PanelCanvas = gameObject.AddComponent<CanvasGroup>();
        }
    }

    void Start()
    {
        if (!isHome)
            OnPanelIn();
        else
            OnPanelPreviousIn();
    }
   
    void OnEnable()
    {
        if (animOnEnable)
            OnPanelIn();
    }

    private void OnPanelPreviousIn()
    {
        //if (MainMenu.Instance == null)
            //return;

        if (isVertical)
        {
            OnPanelIn();
            return;
        }

        var canvasWidth = ParentRect.rect.width;

        /*if (MainMenu.Instance.GetPanelInDirection())
        {
            LeanTween.moveX(PanelRect, 0, 0.3f).setEaseOutCubic().setFrom(canvasWidth);
            LeanTween.alphaCanvas(PanelCanvas, 1, 0.1f);
        }
        else
        {
            LeanTween.moveX(PanelRect, 0, 0.3f).setEaseOutCubic().setFrom(-canvasWidth);
            LeanTween.alphaCanvas(PanelCanvas, 1, 0.1f);
        }*/
    }
    public void OnPanelIn()
    {
        transform.parent.gameObject.SetActive(true);
        LeanTween.move(PanelRect, new Vector2(0,0), 0.3f).setEaseOutCubic();
        LeanTween.alphaCanvas(PanelCanvas, 1, 0.1f);
        isVertical = false;
    }

    public void OnCanvasOut(int direction)
    {
        OnCanvasOut((SlideDirection)direction);
    }
     
    public void OnCanvasOut(SlideDirection direction)
    {
        var canvasWidth = ParentRect.rect.width;
        var canvasHeight = ParentRect.rect.height;

        switch (direction)
        {
            case SlideDirection.Up:
                LeanTween.moveY(PanelRect, canvasHeight, 0.2f).setDelay(0).setOnComplete(DisableCanvas);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                isVertical = true;
                break;
            case SlideDirection.Right:
                LeanTween.moveX(PanelRect, canvasWidth, 0.2f).setDelay(0).setOnComplete(DisableCanvas);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                break;
            case SlideDirection.Down:
                LeanTween.moveY(PanelRect, -canvasHeight, 0.2f).setDelay(0).setOnComplete(DisableCanvas);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                isVertical = true;
                break;
            case SlideDirection.Left:
                LeanTween.moveX(PanelRect, -canvasWidth, 0.2f).setDelay(0).setOnComplete(DisableCanvas);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                break;
        }
    }

    public void OnPanelOut(int direction)
    {
        OnPanelOut((SlideDirection)direction);
    }

    public void OnPanelOut(SlideDirection direction)
    {
        var canvasWidth = ParentRect.rect.width;
        var canvasHeight = ParentRect.rect.height;

        switch (direction)
        {
            case SlideDirection.Up:
                LeanTween.moveY(PanelRect, canvasHeight, 0.2f).setDelay(0).setOnComplete(DisablePanel);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                isVertical = true;
                break;
            case SlideDirection.Right:
                LeanTween.moveX(PanelRect, canvasWidth, 0.2f).setDelay(0).setOnComplete(DisablePanel);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                break;
            case SlideDirection.Down:
                LeanTween.moveY(PanelRect, -canvasHeight, 0.2f).setDelay(0).setOnComplete(DisablePanel);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                isVertical = true;
                break;
            case SlideDirection.Left:
                LeanTween.moveX(PanelRect, -canvasWidth, 0.2f).setDelay(0).setOnComplete(DisablePanel);
                LeanTween.alphaCanvas(PanelCanvas, 0, 0.2f);
                break;
        }
    }


    private void DisableCanvas()
    {
        transform.parent.gameObject.SetActive(false);
    }

    private void DisablePanel()
    {
        gameObject.SetActive(false);
    }
}
