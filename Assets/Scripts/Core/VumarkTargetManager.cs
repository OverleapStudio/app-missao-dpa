using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VumarkTargetManager : DefaultTrackableEventHandler
{
    public VuMarkBehaviour vm;

    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();
        ARMinigameManager.Instance.OnTargetFound(vm.VuMarkTarget.InstanceId.NumericValue);
    }
}
