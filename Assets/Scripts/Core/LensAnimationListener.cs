using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LensAnimationListener : MonoBehaviour
{
    public void OnChallengeStart()
    {
        LensMinigame.Instance.OnChallengeStart();
    }
}
