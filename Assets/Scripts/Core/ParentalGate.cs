using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ParentalGate : MonoBehaviour
{
    public CanvasHome HomeManager;
    public TextMeshProUGUI ParentalMessage;
    public string GateArea = "config";
    public ParentalButton[] parentalButtons;
    private List<int> LockNumbers;
    private int LockCounter = 0;

    private void OnEnable()
    {
        LockCounter = 0;
        foreach(ParentalButton bt in parentalButtons)
        {
            bt.ResetButton();
        }
        SortPassword();
    }

    private void SortPassword()
    {
        LockNumbers = new List<int>();
        int randomNumber;

        for(int i=0; i<3; i++)
        {
            do
            {
                randomNumber = Random.Range(1, 10);
            } while (LockNumbers.Contains(randomNumber));

            LockNumbers.Add(randomNumber);
        }

        ParentalMessage.text = string.Format("Pressione os números: {0}, {1}, {2}.",NumberToString(LockNumbers[0]),NumberToString(LockNumbers[1]),NumberToString(LockNumbers[2]));
    }

    public void OnNeedNumberClick(int index)
    {
        parentalButtons[index - 1].SelectButton();
        LockCounter++;

        if (LockCounter >= 3)
        {
            int correctButtons = 0;

            foreach(ParentalButton bt in parentalButtons)
            {
                if (bt.isSelected() && LockNumbers.Contains(bt.buttonIndex))
                {
                    correctButtons++;
                }
            }

            if(correctButtons >= 3)
            {
                if (GateArea == "need")
                {
                    HomeManager.OnNeedGameButton();
                }
                else
                {
                    HomeManager.OnHelpButton();
                }
                AudioManager.Instance.PlayBtSuccess();
            }
            else
            {
                AudioManager.Instance.PlayBtError();
            }

            gameObject.SetActive(false);
        }
    }

    public string NumberToString(int number)
    {
        switch (number)
        {
            case 1:
                return "um";
            case 2:
                return "dois";
            case 3:
                return "três";
            case 4:
                return "quatro";
            case 5:
                return "cinco";
            case 6:
                return "seis";
            case 7:
                return "sete";
            case 8:
                return "oito";
            case 9:
                return "nove";
            default:
                return "";
        }
    }
}
