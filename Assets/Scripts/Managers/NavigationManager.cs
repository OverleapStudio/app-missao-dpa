using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.SceneManagement;
using System.Linq;

public class NavigationManager : Singleton<NavigationManager>
{
    [SerializeField] private Canvas panelsCanvas = null;

    private Dictionary<string, List<PanelBase>> panelStack = new Dictionary<string, List<PanelBase>>();

    public GameObject defaultScreen;
    public PanelBase CurrentPanel { get; set; }
    private string ActiveScene { get; set; }

    private void Start()
    {
        SceneManager.sceneLoaded += HandleOnLoadScene;
    }

    protected override void OnDestroy()
    {
        SceneManager.sceneLoaded -= HandleOnLoadScene;
        base.OnDestroy();
    }

    public T InstantiateAndShowPanel<T>(string resourceName, Action<T> onBeforeShow, bool animShow = true) where T : PanelBase
    {
        var prefab = Resources.Load(resourceName) as GameObject;
        return InstantiateAndShowPanel<T>(prefab, onBeforeShow);
    }

    public T InstantiateAndShowPanel<T>(GameObject prefab, Action<T> onBeforeShow, bool animShow = true) where T : PanelBase
    {
        var go = Instantiate(prefab, panelsCanvas.transform) as GameObject;
        var component = go.GetComponent<T>();

        onBeforeShow?.Invoke(component);

        var previousPanel = CurrentPanel;

        if (component != null)
        {
            component.Show(animShow, null, () =>
            {
                if (previousPanel != null) previousPanel.gameObject.SetActive(false);
            });

            PushPanel(component);
        }
        else
        {
            Debug.LogError("Component " + typeof(T) + " not found on prefab " + prefab.gameObject.name);
        }

        return component;
    }

    public T ShowPanel<T>(GameObject go, Action<T> onBeforeShow, bool animShow = true) where T : PanelBase
    {
        var component = go.GetComponent<T>();

        onBeforeShow?.Invoke(component);

        var previousPanel = CurrentPanel;

        if (component != null)
        {
            component.Show(animShow, null, () =>
            {
                if (previousPanel != null) previousPanel.gameObject.SetActive(false);
            });

            PushPanel(component);
        }
        else
        {
            Debug.LogError("Component " + typeof(T) + " not found on prefab " + go.gameObject.name);
        }

        return component;
    }

    public void PushPanel(PanelBase panel)
    {
        var activeStack = GetActiveStack();
        CurrentPanel = panel;
        activeStack.Add(panel);

        panel.OnHideStart += p =>
        {
            HandleOnPanelHideStart(activeStack, p);
        };
    }

    public PanelBase PopPanel()
    {
        return PopPanel(GetActiveStack());
    }

    public PanelBase PopPanel(List<PanelBase> stack)
    {
        var popPanel = stack[stack.Count - 1];
        stack.Remove(popPanel);
        if (stack.Count > 0 && stack == GetActiveStack()) CurrentPanel = stack[stack.Count - 1];
        return popPanel;
    }

    public void Hide(int panels = 0, bool anim = false)
    {
        if (panels == 0 || panelStack.Count <= 1)
        {
            panels = 0;
            return;
        }

        CurrentPanel.Hide(anim, true, null, () => Hide(--panels));
    }

    public void HideAll(bool anim = false)
    {
        if (panelStack[ActiveScene].Count == 0) return;
        CurrentPanel.Hide(anim, true, null, () => HideAll());
    }

    private void HandleOnPanelHideStart(List<PanelBase> stack, PanelBase panel)
    {
        if (panel == CurrentPanel)
        {
            PopPanel(stack);
            CurrentPanel.gameObject.SetActive(true);
        }
        else
        {
            stack.Remove(panel);
        }
    }

    private List<PanelBase> GetActiveStack()
    {
        string activeScene;
        activeScene = SceneManager.GetActiveScene().name;

        if (!panelStack.ContainsKey(activeScene))
            panelStack[activeScene] = new List<PanelBase>();
        return panelStack[activeScene];
    }

    private void HandleOnLoadScene(Scene scene, LoadSceneMode loadMode)
    {
        ActiveScene = scene.name;
    }
}