using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RegularMinigameManager : Singleton<RegularMinigameManager>
{
    public GameObject MinigameCanvas;
    public GameObject MenuButtons;
    public List<Minigame> minigames;

    private MinigameBase currentMinigame = null;
    private Minigame currentMinigameData = null;
    private PanelBase currentPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        ShowMinigame(GameManager.Instance.GetMinigame());
    }

    // Update is called once per frame
    public void ShowMinigame(ulong gameIndex)
    {
        currentMinigameData = null;

        foreach(Minigame m in minigames)
        {
            //Debug.Log("Searching " + m.witchName+" vumark "+m._vumark+" for game index"+gameIndex);
            if(m._vumark == gameIndex)
            {
                currentMinigameData = m;
            }
        }

        if (currentMinigameData!=null)
        {
            InstantiateAndShowMinigame(currentMinigameData._resource);
        }
    }

    public void OnMinigameEnded()
    {
        LoaderManager.Instance.OnHiddenLoadingRequest(1);
    }

    public void InstantiateAndShowMinigame(string resourceName)
    {
        var minigame = InstantiateMinigame<MinigameBase>(resourceName);
        minigame.minigameData = currentMinigameData;
        minigame.OnOpenAndShow();
        minigame.onDestroyChallenge += OnMinigameEnded;
        currentMinigame = minigame;
    }

    public T InstantiateMinigame<T>(string resourceName)
    {
        var prefab = Resources.Load(resourceName);
        var go = Instantiate(prefab, MinigameCanvas.transform) as GameObject;
        var component = go.GetComponent<T>();

        return component;
    }

    public T InstantiateMinigame<T> (GameObject prefab)
    {
        var go = Instantiate(prefab, MinigameCanvas.transform) as GameObject;
        var component = go.GetComponent<T>();

        return component;
    }
}
