using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    private ulong MinigameIndex { get; set; }
    public AudioClip mainSong;

    public void StartHome()
    {
        StartCoroutine(LoadHome());
    }

    private void Start()
    {
        StartCoroutine(LoadHome());
    }

    // Start is called before the first frame update
    IEnumerator LoadHome()
    {
        DontDestroyOnLoad(gameObject);
        yield return new WaitForSeconds(1.0f);
        NavigationManager.Instance.InstantiateAndShowPanel<PanelBase>("Canvas-Home", (panel) => {
        });
        AudioManager.Instance.PlaySoundtrack(mainSong,true);
        AudioManager.Instance.SetSoundtrackVolume(0.5f);
        /*if (NavigationManager.Instance.CurrentPanel==null)
        {
        }*/
    }

    public void SetMinigame(ulong index)
    {
        MinigameIndex = index;
    }

    public ulong GetMinigame()
    {
        return MinigameIndex;
    }
}
