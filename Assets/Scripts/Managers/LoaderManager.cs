using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LoaderManager : Singleton<LoaderManager>
{
    public PanelBase PanelLoader;
    public TMP_Text progression;
    private PanelBase previousPanel;

    private int sceneToLoad = -1;

    public void OnLoadingRequest(int sceneToLoad, PanelBase previousPanel)
    {
        /*NavigationManager.Instance.InstantiateAndShowPanel<PanelBase>("Panel-Loader", (panel)=> {
            PanelLoader = panel;
        });*/
        this.previousPanel = previousPanel;
        PanelLoader.Show();
        AudioManager.Instance.SetSoundtrackVolume(0.2f);
        this.sceneToLoad = sceneToLoad;
        StartCoroutine(LoadAsync());
    }

    public void OnHiddenLoadingRequest(int sceneToLoad)
    {
        this.sceneToLoad = sceneToLoad;
        StartCoroutine(HiddenLoadAsync());
        //SceneManager.LoadSceneAsync(sceneToLoad);
    }

    IEnumerator HiddenLoadAsync()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);

        while (asyncLoad.isDone)
            yield return null;

        if(sceneToLoad == 0)
        {
            GameManager.Instance.StartHome();
        }
    }

    IEnumerator LoadAsync()
    {
        if (sceneToLoad == -1) yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);
        asyncLoad.allowSceneActivation = false;

        int progress = 0;
        //yield return new WaitForSeconds(3.0f);

        // Wait until the asynchronous scene fully loads
        while (progress<100 || asyncLoad.isDone)
        {
            progression.text = progress.ToString()+"%";
            progress++;
            yield return new WaitForSeconds(Random.Range(0.01f,0.08f));
        }

        asyncLoad.allowSceneActivation = true;

        OnLoadingDone();
    }

    public void OnLoadingDone()
    {
        previousPanel?.Hide();
        PanelLoader?.Hide(false);
        AudioManager.Instance.SetSoundtrackVolume(0.5f);
    }
}
