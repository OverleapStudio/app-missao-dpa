using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource Soundtrack;
    public AudioSource Voice;
    public AudioSource SFX;
    public AudioButton AudioButtons;

    public void PlaySFX(AudioClip sfx)
    {
        SFX.PlayOneShot(sfx);
    }

    public void PlaySoundtrack(AudioClip soundtrackClip, bool hasLoop)
    {
        Soundtrack.time = 0;
        Soundtrack.clip = soundtrackClip;
        Soundtrack.loop = hasLoop;
        Soundtrack.Play();
    }

    public void PlaySoundtrack(AudioClip soundtrackClip)
    {
        Soundtrack.time = 0;
        Soundtrack.clip = soundtrackClip;
        Soundtrack.Play();
    }

    public void PlaySoundtrack()
    {
        Soundtrack.Play();
    }

    public void StopSoundtrack()
    {
        Soundtrack.Stop();
    }

    public void SetSoundtrackVolume(float volume)
    {
        Soundtrack.volume = volume;
    }

    public void PlayVoice(AudioClip voiceClip)
    {
        Voice.time = 0;
        Voice.clip = voiceClip;
        Voice.Play();
    }

    public void PlayVoice()
    {
        if(Voice.time >= Voice.clip.length)
        {
            Voice.time = 0;
        }
        Voice.Play();
    }

    public void PauseVoice()
    {
        Voice.Pause();
    }

    public void StopVoice()
    {
        Voice.Stop();
    }

    public void RestartVoice()
    {
        Voice.time = 0;
        Voice.Play();
    }

    public bool GetSFXStatus()
    {
        return Voice.mute;
    }

    public bool GetBGMStatus()
    {
        return Soundtrack.mute;
    }

    public void MuteSFX()
    {
        Voice.mute = !Voice.mute;
        SFX.mute = !SFX.mute;
    }

    public void MuteBGM()
    {
        Soundtrack.mute = !Soundtrack.mute;
    }

    public void PlayBtSuccess()
    {
        SFX.PlayOneShot(AudioButtons.success);
    }

    public void PlayBtError()
    {
        SFX.PlayOneShot(AudioButtons.error);
    }

    public void PlayBtClick()
    {
        SFX.PlayOneShot(AudioButtons.click);
    }
}

[System.Serializable]
public class AudioButton
{
    public AudioClip click;
    public AudioClip success;
    public AudioClip error;
}
