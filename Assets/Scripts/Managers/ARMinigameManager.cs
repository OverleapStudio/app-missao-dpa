using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Vuforia;

public class ARMinigameManager : Singleton<ARMinigameManager>
{
    [Header("AR Minigame")]
    public GameObject ARRoot;
    public GameObject ARShuffleObject;
    public GameObject ARMageCanvas;
    public GameObject ARMagicOrb;
    public UnityEngine.UI.Image ARMage;
    public Sprite maleMage;
    public Sprite femaleMage;
    public Animator ARCaptureAnimator;

    public delegate void OnARInteraction();
    public event OnARInteraction onARInteraction;

    private int ARIndex = -1;

    public void ARButton(int ARButton)
    {
        Debug.Log("AR Button triggered");
        ARIndex = ARButton;
        if (onARInteraction != null)
        {
            onARInteraction();
        }
    }

    public int GetARIndex()
    {
        return ARIndex;
    }

    [Header("General AR")]
    public GameObject ARCanvas;
    public GameObject ARFx;
    public GameObject ARCaptureScreen;
    public GameObject HomeDialog;
    public AudioClip ARSoundtrack;

    private MinigameBase currentMinigame = null;
    private Minigame currentMinigameData = null;

    public List<Minigame> minigames;
    private bool hasCaptured = false;

    // Start is called before the first frame update
    void Start()
    {
        ShowAR();
        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
        //Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnVuforiaStarted()
    {
        CameraDevice.Instance.SetFocusMode(
        CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    private void OnPaused(bool paused)
    {
        // Set again autofocus mode when app is resumed
        CameraDevice.Instance.SetFocusMode(
        CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }

    //SUBSTITUIR PELO DIALOG MODULAR DEPOIS
    public void ShowHomeDialog()
    {
        HomeDialog.SetActive(true);
    }

    public void OnConfirmHome()
    {
        LoaderManager.Instance.OnHiddenLoadingRequest(0);
    }

    public void ShowAR()
    {
        ARCaptureScreen.SetActive(true);
        AudioManager.Instance.PlaySoundtrack(ARSoundtrack, false);
    }

    public void OnTargetFound(ulong index)
    {
        //Debug.Log("Vumark " + index + " found.");

        if (currentMinigame == null)
        {
            hasCaptured = true;
            GameManager.Instance.SetMinigame(index);
            currentMinigame = null;

            foreach (Minigame m in minigames)
            {
                if(m._vumark == index)
                {
                    //Debug.Log(index + "found");
                    //TEMPORARY DISABLE AR Interaction
                    //if (m._resource.Contains("Stone")) return;
                    //AudioManager.Instance.PlaySFX(BatsFX);
                    //ARBats.SetActive(true);
                    //ARCaptureScreen.SetActive(false);
                    currentMinigameData = m;
                }
            }
            StartCoroutine(ARTransition());
        }
    }

    public void OnAREnded()
    {
        hasCaptured = false;
        ARCaptureAnimator.SetTrigger("end");
        currentMinigame = null;
        currentMinigameData = null;
        ARMageCanvas.SetActive(false);
    }

    public void SetMageCanvas(bool ARActive)
    {
        ARMageCanvas.SetActive(ARActive);
    }

    public void SetMagicOrb(bool ARActive)
    {
        ARMagicOrb.SetActive(ARActive);
    }

    IEnumerator ARTransition()
    {
        ARFx.SetActive(true);
        ARCaptureAnimator.SetTrigger("start");
        yield return new WaitForSeconds(3.0f);
        if (currentMinigameData && currentMinigameData.hasCamera)
        {
            InstantiateAndShowMinigame(currentMinigameData._resource);
            ARMageCanvas.SetActive(true);
            currentMinigame.onDestroyChallenge += OnAREnded;
        }
        else
        {
            LoaderManager.Instance.OnHiddenLoadingRequest(2);
        }
        ARFx.SetActive(false);
        ARCaptureScreen.SetActive(false);
    }

    public void OnMinigameEnded()
    {
        //ARLoop.Play();
        if (ARShuffleObject != null) Destroy(ARShuffleObject);
        ARCaptureScreen.SetActive(true);
        AudioManager.Instance.PlaySoundtrack(ARSoundtrack, false);
        currentMinigame = null;
        currentMinigameData = null;
        ARMageCanvas.SetActive(false);
    }

    public void SetARMage(bool isFemale)
    {
        ARMage.sprite = isFemale ? femaleMage : maleMage;
    }

    public Animator InstantiateShuffleObject(string resourceName)
    {
        var prefab = Resources.Load(resourceName);
        ARShuffleObject = Instantiate(prefab, ARRoot.transform) as GameObject;
        ARMageCanvas.SetActive(true);

        return ARShuffleObject.GetComponent<Animator>();
    }

    public void InstantiateAndShowMinigame(string resourceName)
    {
        var minigame = InstantiateMinigame<MinigameBase>(resourceName);
        minigame.minigameData = currentMinigameData;
        minigame.OnOpenAndShow();
        minigame.onDestroyChallenge += OnMinigameEnded;
        currentMinigame = minigame;
    }

    public T InstantiateMinigame<T>(string resourceName)
    {
        var prefab = Resources.Load(resourceName);
        var go = Instantiate(prefab, ARCanvas.transform) as GameObject;
        var component = go.GetComponent<T>();

        return component;
    }

    public T InstantiateMinigame<T> (GameObject prefab)
    {
        var go = Instantiate(prefab, ARCanvas.transform) as GameObject;
        var component = go.GetComponent<T>();

        return component;
    }
}
