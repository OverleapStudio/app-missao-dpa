﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorUtils
{
    public static HSLColor HSLfromColor(Color rgb){
        // Convert RGB to a 0.0 to 1.0 range.
        double double_r = rgb.r;
        double double_g = rgb.g;
        double double_b = rgb.b;
        double h, s, l;

        // Get the maximum and minimum RGB components.
        double max = double_r;
        if (max < double_g) max = double_g;
        if (max < double_b) max = double_b;

        double min = double_r;
        if (min > double_g) min = double_g;
        if (min > double_b) min = double_b;

        double diff = max - min;
        l = (max + min) / 2;
        if (Math.Abs(diff) < 0.00001)
        {
            s = 0;
            h = 0;  // H is really undefined.
        }
        else
        {
            if (l <= 0.5) s = diff / (max + min);
            else s = diff / (2 - max - min);

            double r_dist = (max - double_r) / diff;
            double g_dist = (max - double_g) / diff;
            double b_dist = (max - double_b) / diff;

            if (double_r == max) h = b_dist - g_dist;
            else if (double_g == max) h = 2 + r_dist - b_dist;
            else h = 4 + g_dist - r_dist;

            h = h * 60;
            if (h < 0) h += 360;
        }

        return new HSLColor((float)h,(float)s,(float)l);
    }

    public static Color ColorFromHSL(HSLColor hsl)
    {
        double h = hsl.h, s = hsl.s, l = hsl.l;
        double r, g, b;

        double p2;
        if (l <= 0.5) p2 = l * (1 + s);
        else p2 = l + s - l * s;

        double p1 = 2 * l - p2;
        double double_r, double_g, double_b;
        if (s == 0)
        {
            double_r = l;
            double_g = l;
            double_b = l;
        }
        else
        {
            double_r = QqhToRgb(p1, p2, h + 120);
            double_g = QqhToRgb(p1, p2, h);
            double_b = QqhToRgb(p1, p2, h - 120);
        }

        // Convert RGB to the 0 to 255 range.
        r = (int)(double_r * 255.0);
        g = (int)(double_g * 255.0);
        b = (int)(double_b * 255.0);

        return new Color((float)(r/255.0),(float)(g/255.0),(float)(b/255.0),1);
    }

    private static double QqhToRgb(double q1, double q2, double hue)
    {
        if (hue > 360) hue -= 360;
        else if (hue < 0) hue += 360;

        if (hue < 60) return q1 + (q2 - q1) * hue / 60;
        if (hue < 180) return q2;
        if (hue < 240) return q1 + (q2 - q1) * (240 - hue) / 60;
        return q1;
    }

    public static Color ColorFromHue(HSLColor hsl){
        return ColorFromHSL(new HSLColor(hsl.h,1,0.5f));
    }

}

public struct HSLColor
{
    public float h, s, l;

    public HSLColor(float h, float s, float l) : this()
    {
        this.h = h;
        this.s = s;
        this.l = l;
    }

    override public string ToString()
    {
        return string.Format("( {0:F}, {1:F}, {2:F} )", h, s * 100, l * 100);
    }
}